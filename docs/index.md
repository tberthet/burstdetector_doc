# Home

Le module  **BurstDetector** est un genre de Radiogaga sur tout NenuFAR. Il s'agit d'un trigger permettant de détecter des phénomènes impulsifs.
Contrairement à Radiogaga il peut trigger sur chaque MR indépendant cad. dans n'importe quel direction. ( L'impact du beam analogique a tout de même un effet).
Le principe est le suivant :

![Schéma de prince](figures/BurstDetector.png)