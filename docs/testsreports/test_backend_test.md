# Test sur backend de test

Pour tester le bon fonctionnement du module le Firmware a été flashé sur 2 cartes du backend de tests.

Le test a été réalisé durant le 25 et 26 octobre 2022. 
Plusieurs dizaines de milliers d'événements ont étés détectés. 

!!! check "Bilan"

    - Le trigger permet bien de trigger dans n'importe quelles directions. Cependant le phasage analogique a bien un impact sur la détection. En effet certains événement sont visibles uniquement sur une plage de temps correspondant exactement à une observation.  
    - Le soft de TBB semble bien se comporter malgré le nombre de trigs plus importants. **A surveiller avec tout le backend de prod ! **  
    - Peut être jouer sur la latence et le seuil afin de ne pas se faire arroser par une unique source.



Voici le résumé de chaque journée : 

![25 octobre](../figures/20221025.png)
![26 octobre](../figures/20221026.png)
