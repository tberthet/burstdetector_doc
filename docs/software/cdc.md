# Cahiers des charges

## Chargement d'un fichier de commandes par carte
Pour la configuration de ce module, cela ressemble beaucoup à Radiogaga. 

Il faudra charger sur chaque carte un fichier de commande afin de :

- Configurer les filtres
- Configurer K (seuil)
- Activer le tramer UDP
  
!!! question
    Exécution des commandes au démarrage du service ? Ou à la modification du fichier, comme Radiogaga permettant de faire des modifs en temps réel ? 

## Développement annexes

### Desactivation lors d'OBS Radiogaga 

Désactivation du module burstDetector durant les OBS Radiogaga **(assez urgent)**.  
Un des premiers points qu'il faudrait intégrer en plus. Est de désactiver l'envoi de trig lors d'observation où Radiogaga fonctionne. Histoire de ne pas saturer l'acquisition TBB par risque de manquer des événements. ( limite à 10 événements par seconde)
Donc il faudrait ecrire **'0'** dans un registre **d'UDP_sender** puis remettre **'1'** à la fin de l'observation :

```c
c_udp_sender_burstDetector_address_offset + c_UDP_sender_enable_offset
//16#0098_0000#                           + 16#07#
```

### Statistiques du nombre de trigs sur chaque MR
Une autre fonctionnalité. Serait de monitorer le nombre de trigs sur chaque MR.
Pour cela il faudrait lire 2 x 4  registres sur chacune des cartes, aux adresses suivantes : 

``` c 
// X 
c_processing_address_offset          //  16#0103_0000#
+ c_burstDetector_address_offset     //  16#0031_0000#  
+ c_trigger_address_offset           //  16#0006_0000#
+ c_trigger_cnt_ok_x_offset + 0 to 3 //  16#C0#

// Y
c_processing_address_offset          //  16#0103_0000#   
+ c_burstDetector_address_offset     //  16#0031_0000#      
+ c_trigger_address_offset           //  16#0006_0000#
+ c_trigger_cnt_ok_y_offset + 0 to 3 //  16#D0#
```
!!! question
    Peut etre besoin de monitor/SGDMA ? 